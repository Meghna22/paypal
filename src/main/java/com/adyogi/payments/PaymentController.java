package com.adyogi.payments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;


import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;



@RestController
@CrossOrigin
@RequestMapping("/auth")
public class PaymentController extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
        .authorizeRequests()
            .antMatchers("/**").permitAll();
		
		//http.headers().contentSecurityPolicy("connect-src 'self' wss://* https://* uploads.intercomcdn.com api-iam.intercom.io static.intercomassets.com static.intercomassets.com nexus-websocket-a.intercom.io nexus-websocket-b.intercom.io app.getsentry.com").reportOnly();
	//http.headers().addHeaderWriter(new StaticHeadersWriter("X-Content-Security-Policy","connect-src 'self' wss://* https://* uploads.intercomcdn.com api-iam.intercom.io static.intercomassets.com static.intercomassets.com nexus-websocket-a.intercom.io nexus-websocket-b.intercom.io app.getsentry.com"));
	}
	
	
	public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/*").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT")
                .allowedHeaders("Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method",
                        "Access-Control-Request-Headers")
                .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
                .allowCredentials(true).maxAge(3600);
    }
	
	@RequestMapping(value = "/home1")
	public String index(){
		System.out.println("Hello!");
		return "index"; 
	}
	
		
	@RequestMapping(value = "/home")
	public  String redirectWithRedirectView(@RequestParam("budget") int budget,@RequestParam("margin") int margin,
			@RequestParam("client_id") String client_id,@RequestParam("plan_id") int plan_id,@RequestParam("referrer") String referrer) throws Exception {
		
		
		
		System.out.println("budget-"+budget);
		System.out.println("margin-"+margin);
		System.out.println("client_id-"+client_id);
		System.out.println("plan_id-"+ plan_id);
		System.out.println("referrer-"+referrer);
		
		String fees = String.valueOf(margin);
		System.out.println("fees-"+fees);
		
		
		
		//Retrieve access token
		
		String url = "https://api.paypal.com/v1/oauth2/token";

		RestTemplate restTemplate = new RestTemplate();

		
		String username= "ATD8GmpvMgOlyMmsZmgJdEp7cPFYLNWbCvpEnuCSWalvAxcJSZHjUGwmQEgbqEcnVqsvHeMmju5PTfIX";
		String password= "EIxFFYRoghkurvDm3tdqI8wdarULbUjM1hhMjlO3BvVtbQJxppOxXuo5r0lWdxR9SjkDnvoUGTlPL9QP";
		
		
		 String auth = username + ":" + password;
         byte[] encodedAuth = Base64.encodeBase64( 
            auth.getBytes(Charset.forName("US-ASCII")) );
         String authHeader = "Basic " + new String( encodedAuth );
         System.out.println("authheader:  "+ authHeader);
         
		

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set( "Authorization", authHeader );
		System.out.println("headers:  "+headers);
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "client_credentials");
		

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		String answer = restTemplate.postForObject(url, request, String.class);
		JSONObject tokenJson = new JSONObject(answer);
		System.out.println(answer);
		

		String access_token = tokenJson.getString("access_token");
		System.out.println(access_token);
		
		//Post request to PayPal to create a payment
		
		String payment_url = "https://api.paypal.com/v1/payments/payment";

		RestTemplate payment_restTemplate = new RestTemplate();
		
		JSONObject object1 = new JSONObject();
		object1.put("return_url", "https://app.adyogi.com/paypal-success");
		object1.put("cancel_url", "https://app.adyogi.com/paypal-failure");
		
		JSONObject object2 = new JSONObject();
		object2.put("payment_method", "paypal");
		
		JSONObject object3 = new JSONObject();
		object3.put("total", fees);
		object3.put("currency", "USD");
		
		JSONObject amount= new JSONObject();
		amount.put("amount",object3);
		
		JSONArray array =new JSONArray();
		array.put(amount);
		System.out.println(array);
	

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("intent","sale");
		jsonObject.put("redirect_urls",object1);
		jsonObject.put("payer",object2);
		jsonObject.put("transactions", array);
		
		System.out.println(jsonObject);
		
		
		HttpHeaders payment_headers = new HttpHeaders();
		payment_headers.setContentType(MediaType.APPLICATION_JSON);
		payment_headers.set("Authorization","Bearer "+access_token);
		System.out.println("headers:  "+payment_headers);
		
		

		HttpEntity<String> entity =new HttpEntity<String>(jsonObject.toString(), payment_headers);
		String payment_answer = payment_restTemplate.postForObject(payment_url, entity, String.class);
		JSONObject payment_tokenJson = new JSONObject(payment_answer);
		System.out.println(payment_tokenJson);
		

		String id = payment_tokenJson.getString("id");
		System.out.println(id);
		
		JSONArray links = payment_tokenJson.getJSONArray("links");
		JSONObject object = links.getJSONObject(1);
		System.out.println(object);
		String href =object.getString("href");
		System.out.println(href);
		
		
		// Store payment_id to back4app Payments table
		
		String application_id = "0f5uiNLZwPItPmo2g10gQlaCpglrbzkpuRnXUSqp";
		String rest_api_key = "lNPIUzmgFLoLbX4F1YqJ9RhRFRd7gfOwOM3OugrG";
		String session_token = "r:72c09c02958e95e133f9e1be6e2b5099";
		
		
		String CUSTOM_URL = "https://parseapi.back4app.com/classes/Payments";

		HttpClient postClient = new DefaultHttpClient();
		HttpPost post = new HttpPost(CUSTOM_URL);

		post.setHeader("X-Parse-Application-Id", application_id);
		post.setHeader("X-Parse-REST-API-Key", rest_api_key);
		post.setHeader("X-Parse-Session-Token", session_token);

		JSONObject payload = new JSONObject();
		payload.put("budget",budget);
		payload.put("margin", margin);
		payload.put("amount", margin);
		payload.put("client_id", client_id);
		payload.put("plan_id", plan_id);
		payload.put("plan_duration", 1);
		payload.put("status", 0);
		payload.put("channel", "PayPal");
		payload.put("currency", "USD");
		payload.put("referrer", referrer);
		payload.put("payment_id", id);
		payload.put("access_token", access_token);
		
		System.out.println(payload);
		

		StringEntity requestEntity = new StringEntity(payload.toString(),ContentType.APPLICATION_JSON);
		post.setEntity(requestEntity);

		postClient.execute(post);
		
		
		//Execute the payment
	/*	String execute_url="https://api.paypal.com/v1/payments/payment/" +id +"/execute/" ;
		
		getQueryMap(return_url);
		
		JSONObject execute_object = new JSONObject();
		execute_object.put("payer_id", payer_id);
		
		HttpEntity<String> execute_entity =new HttpEntity<String>(execute_object.toString(), payment_headers);
		String execute_answer = payment_restTemplate.postForObject(execute_url, execute_entity, String.class);
		JSONObject execute_tokenJson = new JSONObject(execute_answer);
		System.out.println(execute_tokenJson);
		

		String id = payment_tokenJson.getString("id");
		System.out.println(id);*/
		
		
		/*RedirectView redirectView = new RedirectView();
		redirectView.setUrl(href);
		System.out.println(redirectView);*/
		return href;
	}
	
	


}
