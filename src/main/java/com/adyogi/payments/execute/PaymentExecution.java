package com.adyogi.payments.execute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
@RequestMapping("/payment")
public class PaymentExecution {

	static String payer_id="";

	

	/**
	 * 
	 * @param accessToken
	 *            The access token of shop
	 * @param url
	 *            The URL to fetch data from
	 * @return The response from shopify encapsulated in another responsevo json
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 */



	@RequestMapping(value= "/execute")
	public  RedirectView fetch(@RequestParam("id") String id,
			@RequestParam("return_url") String return_url,@RequestParam("accesstoken") String accesstoken ){

		try {
			//Execute the payment
			String execute_url="https://api.paypal.com/v1/payments/payment/" +id +"/execute/" ;

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders payment_headers = new HttpHeaders();
			payment_headers.setContentType(MediaType.APPLICATION_JSON);
			payment_headers.set("Authorization","Bearer "+accesstoken);
			System.out.println("headers:  "+payment_headers);

			getQueryMap(return_url);

			JSONObject execute_object = new JSONObject();

			execute_object.put("payer_id", payer_id);


			HttpEntity<String> execute_entity =new HttpEntity<String>(execute_object.toString(), payment_headers);
			String execute_answer = restTemplate.postForObject(execute_url, execute_entity, String.class);
			JSONObject execute_tokenJson = new JSONObject(execute_answer);
			System.out.println(execute_tokenJson);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RedirectView redirectView = new RedirectView();
		redirectView.setUrl("/");
		System.out.println(redirectView);
		return redirectView;



	}


	public static String getQueryMap(String query)  
	{  
		MultiValueMap<String, String> parameters =
				UriComponentsBuilder.fromUriString(query).build().getQueryParams();
		List<String> param1 = parameters.get("paymentId");//paymentToken
		List<String> param2 = parameters.get("PayerID");


		System.out.println("param1: " + param1.get(0));
		System.out.println("param2: " + param2.get(0));


		payer_id = param2.get(0);

		return payer_id;  
	}

}
