package com.adyogi.payments.execute;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This VO captures the response from Shopify.
 * 
 * @author Nimish
 *
 */
/*@Configuration
@EnableWebMvc*/
public class PaymentExecutionResponseVO {
	
	String status;
	String response;
	
	/*@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
				.allowedOrigins("*")
				.allowedMethods("GET","PUT")
					.allowedHeaders("Access-Control-Allow-Origin")
				.allowCredentials(false);
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS").allowedOrigins("*")
                .allowedHeaders("*");
		}
		};
	}*/
	/**
	 * 
	 * @return http response code
	 */
	//@CrossOrigin
	public String getStatus() {
		return status;
	}
	//@CrossOrigin
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * 
	 * @return response from shopify
	 */
//	@CrossOrigin
	public String getResponse() {
		return response;
	}
//	@CrossOrigin
	public void setResponse(String response) {
		this.response = response;
	}
	
	

}
